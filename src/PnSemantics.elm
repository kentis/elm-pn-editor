module PnSemantics exposing (fireTransition,transitionCanFire)

import Types exposing (..)

fireTransition: Model -> Int -> Model
fireTransition model id =
    if (transitionCanFire model id) then
        (fire model id)
    else
        model
transitionCanFire: Model -> Int -> Bool
transitionCanFire model id = 
    case List.filter (\x -> x.id == id && x.nodeType == TRANSITION) model.model.nodes of
      node::[] ->
        placesAreLoaded model node
      _ ->
        False

placesAreLoaded: Model ->  Node -> Bool
placesAreLoaded model node = 
    let
      ids = (List.map (\x -> x.source)
    <| List.filter (\x -> x.target == node.id) 
    <| model.model.arcs)
    in  
        List.isEmpty ids || List.all (placeHasToken model) ids


placeHasToken: Model -> Int -> Bool
placeHasToken model id =
    case (List.filter (\x -> x.id == id && x.nodeType == PLACE) model.model.nodes) of
      node::[] ->
        node.tokens > 0
      _ ->
        False

fire: Model -> Int -> Model
fire model id = 
    let 
        modelDef = model.model
    in
        {model | model = {modelDef | nodes=(addTokensToTargets modelDef id) } }



addTokensToTargets: ModelDef -> Int -> List Node
addTokensToTargets model id=
    let
      targetIds = List.map (\x -> x.target) (List.filter (\x -> x.source == id) model.arcs)
      sourceIds = List.map (\x -> x.source) (List.filter (\x -> x.target == id) model.arcs)
    in
      List.map (\node -> 
        if(List.any (\x -> node.id == x) targetIds) then
            {node | tokens=(node.tokens + 1)}
        else
            if(List.any (\x -> node.id == x) sourceIds) then
                {node | tokens=(node.tokens - 1)}
            else 
                node
      )  model.nodes