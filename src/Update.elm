module Update exposing (..)
import Mouse exposing(position, Position)
import Types exposing (..)
import PnSemantics exposing(fireTransition)

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
      ClickNode n->
        ({model | selected = n }, Cmd.none)
      DragStart id position->
        ({model | dragged = id, drag = (Just (Drag position position)), dragType = DRAG_NODE}, Cmd.none)
      DragBreakPointStart id bpid position->
            ({model | dragged = id, drag = (Just (Drag position position)), draggedBp=bpid, dragType = DRAG_BREAKPOINT}, Cmd.none)
      DragAt dragType position ->
        case model.drag of
          Just drag ->
            let 
              modelDef = model.model
            in
              case model.dragType of
                DRAG_NODE ->
                  ({model | model=( {modelDef| nodes= (updatePosition model position drag)} )}, Cmd.none)  
                DRAG_BREAKPOINT ->
                  ({model | model=( {modelDef| arcs= (updateBpPosition model position drag)} )}, Cmd.none)  
                NONE ->
                  ( model, Cmd.none ) 
          Nothing ->
            ( model, Cmd.none )    
      DragEnd pos -> 
         ({model | drag = Nothing, dragType = NONE }, Cmd.none)
      ResizeStart id pos ->
        let
          mNode = List.head (List.filter (\x -> x.id == id) model.model.nodes )
        in
          case mNode of
            Just node ->
              ({model | resized = id, resize=(Just (ReSize pos node))}, Cmd.none)
            _ ->
              (model, Cmd.none) 
      ResizeAt pos -> ( (applyResize model pos), Cmd.none)
      ResizeEnd pos -> 
        ({model | resize = Nothing }, Cmd.none)
      ApplyTool  elemId pos ->
        ((applyTool model pos elemId), Cmd.none)
      SelectElement id ->
        ({model | pallette = (updatePallette model.pallette id)}, Cmd.none )--| elements = (List.map (\e -> {e | e.selected = (e.id == id)}) model.pallette.elements )}})
      SelectArc id ->
        let 
          modelDef = model.model
        in
          ({model| model={modelDef | nodes=(List.map (selectNodeIfMatch -1) modelDef.nodes), arcs=(List.map (selectArcIfMatch id) modelDef.arcs) }}, Cmd.none)
      ChangeLabel id value ->
        let 
          modelDef = model.model
        in
          ({model | model={modelDef | nodes=(updateLabel model id value)}}, Cmd.none)
      ChangeTokens id value ->
        let 
          modelDef = model.model
        in
          ({model | model={modelDef | nodes=(updateTokens model id value)}}, Cmd.none)
      AddBreakPoint id pos ->
        let 
          modelDef = model.model
        in
          ({model | model={modelDef | arcs=(addBreakPoint id pos modelDef.arcs)}}, Cmd.none)
      Fire id ->
        (fireTransition model id, Cmd.none)
      _ ->        
        ( model, Cmd.none )


addBreakPoint: Int -> Position -> List ArcDef -> List ArcDef
addBreakPoint id pos arcs= 
    List.map (\x -> case x.id == id of
                      True ->
                          {x | breakPoints=(newBreakPoint pos x.breakPoints )::x.breakPoints}
                      False ->
                        x
              ) arcs
        

newBreakPoint: Position -> List BreakPoint -> BreakPoint
newBreakPoint position  breakPoints =
  let 
    nextId = case List.maximum (List.map (\x -> x.id) breakPoints) of
                Just n ->
                  n +1
                Nothing ->
                  1            
  in 
    {id=nextId, x=(position.x), y=(position.y)}


updateLabel model id value =
  model.model.nodes
  |> List.map (\x ->
    case x.id == id of
      True ->
        {x| label=value}
      _ ->
        x
  )
updateTokens: Model->Int->String->List Node
updateTokens model id value =
  let
    val = String.toInt value
  in
    case val of
      Ok v ->
        model.model.nodes
        |> List.map (\x ->
          case x.id == id of
            True ->
              {x| tokens=v}
            _ ->
              x
          )
      _ ->
        model.model.nodes
    

applyResize: Model -> Position -> Model
applyResize model pos =
  case model.resize of
    Just drag ->
        let 
          modelDef = model.model
        in
           {model | model=( {modelDef| nodes= (updateSize model pos drag)} )}
    Nothing ->
      model


updateSize: Model -> Position -> ReSize -> List Node
updateSize model pos resize =
  model.model.nodes
    |> List.map (updateNodeSizeIfMatch model.resized pos resize model)

updateNodeSizeIfMatch: Int -> Position -> ReSize -> Model -> Node -> Node
updateNodeSizeIfMatch id position resize model node =
    case node.id == id of
      True ->
        {node | width = (resize.startNode.width) + (position.x - resize.start.x),  height=(resize.startNode.height) + (position.y - resize.start.y)}
      False ->
        node


applyTool: Model -> Position -> Maybe Int -> Model
applyTool model pos elemId =
  let
    tool = getToolFromPallette model.pallette
    modeldef = model.model
  in
        case tool of
          Just CREATE_NODE ->
            {model | pallette = (updatePallette model.pallette -1), model=(updateModelDefNodes model.model (updateEditiorNewNodeByTool model pos))}  
          Just CREATE_ARC ->
            case elemId of
              Just id ->
                case model.arcStart of
                  Just startId->
                    {model | arcStart = Nothing, 
                            pallette = (updatePallette model.pallette -1), 
                            model={modeldef | arcs=model.model.arcs++[{id=(getNextId model.model.arcs),  source=startId, target=id, isSelected=False, breakPoints=[]}] } }
                  Nothing ->
                    {model | arcStart = (Just id)}
              Nothing ->
                {model | pallette = (updatePallette model.pallette -1)} 
          Nothing -> --No tool means select element
            case elemId of
              Just id ->
                {model | model={modeldef | nodes=(List.map (selectNodeIfMatch id) model.model.nodes),  arcs=(List.map (selectArcIfMatch -1) model.model.arcs)} }
              _ ->
                {model | model={modeldef | nodes=(List.map (selectNodeIfMatch -1) model.model.nodes), arcs=(List.map (selectArcIfMatch -1) model.model.arcs)} }


getNextId: List ArcDef -> Int
getNextId arcs =
  let
    currentMax = List.maximum 
                 <| List.map (\x -> x.id) arcs
  in 
    case currentMax of
      Just n ->
        n+1
      _ ->
        1


selectNodeIfMatch: Int -> Node -> Node
selectNodeIfMatch id node=
  {node | isSelected = (node.id == id) }

selectArcIfMatch: Int -> ArcDef -> ArcDef
selectArcIfMatch id arc=
  {arc | isSelected = (arc.id == id) }

getToolFromPallette: PalletteDef -> Maybe Tool
getToolFromPallette pallette =
    let 
      element = List.head <| List.filter (\x -> x.selected) pallette.elements
     in
        case element of
          Just e ->
            Just e.tool
          option2 ->
            Nothing
               


updateModelDefNodes modelDef nodeList =
  {modelDef | nodes=nodeList}

updateEditiorNewNodeByTool: Model -> Position -> List Node
updateEditiorNewNodeByTool model pos =
  let
    tool = List.head <|List.filter (\x -> x.selected) model.pallette.elements
    nextId = case (List.maximum <| List.map (\x -> x.id ) model.model.nodes) of
             Just n ->
               n+1
             Nothing ->
               1
  in
    case tool of
      Just t ->
        let
          newNode = {id=nextId, label="", tokens=0, nodeType = t.nodeType, x=pos.x-model.editor.x, y=pos.y-model.editor.y, width=10, height=10, isSelected = False, isDragged = False}
        in
          newNode :: model.model.nodes
      _ ->
       model.model.nodes

updatePallette pallette id =
  {pallette | elements=(List.map (\e-> {e | selected = (e.id == id)}) pallette.elements)}

updateBpPosition: Model -> Position -> Drag -> List ArcDef
updateBpPosition model position drag =
    model.model.arcs
    |> List.map (updateArcBpIfMatch model.dragged model.draggedBp position model)


updateArcBpIfMatch: Int ->  Int -> Position -> Model -> ArcDef -> ArcDef
updateArcBpIfMatch arcId bpId pos model arc =
  case arcId == arc.id of
     True ->
      {arc | breakPoints=(List.map (updateBpIfMAtch bpId pos model) arc.breakPoints)}
     _ ->
      arc
  
updateBpIfMAtch: Int -> Position -> Model -> BreakPoint -> BreakPoint
updateBpIfMAtch bpId pos model bp =
  case bp.id == bpId of
    True ->
      {bp | x = pos.x, y = pos.y}
    _ ->
      bp


updatePosition: Model -> Position -> Drag -> List Node
updatePosition model position drag =
    model.model.nodes
    |> List.map (updateNodeIfMatch model.dragged position model)

updateNodeIfMatch: Int -> Position -> Model -> Node -> Node
updateNodeIfMatch id position model node =
    case node.id == id of
      True ->
        {node | x = position.x - model.editor.x, y = position.y  - model.editor.y}
      False ->
        node

subscriptions : Model -> Sub Msg
subscriptions model =
  case model.drag of
    Nothing ->
      case model.resize of
        Just _ ->
          Sub.batch [ Mouse.moves ResizeAt, Mouse.ups ResizeEnd ]
        Nothing ->
          Sub.none
    Just _ ->
      Sub.batch [ Mouse.moves (DragAt model.dragType), Mouse.ups DragEnd ]


