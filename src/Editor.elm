module Editor exposing (..)
import Html exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Html.Events exposing (..)
import Update exposing (..)
import Mouse exposing (position, Position)
import Json.Decode as Decode
import Types exposing (..)


editorView : Model -> Html.Html Msg 
editorView model = 
    svg [
        width (toString model.editor.width)
        , height (toString model.editor.height)
        , Svg.Attributes.style (" position: absolute; left: "++ (toString model.editor.x) ++"px; top:"++(toString model.editor.y) ++"px;   ")
        , viewBox "0 0 1200 1000"
        , onEditorClick Nothing] 
        ([ 
          marker [id "arrow", markerWidth "10", markerHeight "10", refX "8", refY "3", orient "auto", markerUnits "strokeWidth"] [
            Svg.path [d "M0,0 L0,6 L9,3 z", fill "red"] []
          ]
        ] ++ (List.concat (List.map renderNode model.model.nodes)) 
          ++ (List.concat (List.map (renderArc  model model.model.nodes) model.model.arcs)) 
        )

renderArc : Model -> List Node -> ArcDef -> List (Svg.Svg Msg)
renderArc model nodes arc =
    let
      startNode = case (List.filter (\x -> x.id == arc.source) nodes) of
                      n :: rest ->
                        Just n
                      option2 ->
                        Nothing
      targetNode = case (List.filter (\x -> x.id == arc.target) nodes) of
                      n :: rest ->
                        Just n
                      option2 ->
                        Nothing
    in
        case startNode of
             Just sn ->
               case targetNode of
                 Just tn ->
                   [
                     --line [x1 (toString sn.x), y1 (toString sn.y),  x2 (toString tn.x), y2 (toString tn.y), Svg.Attributes.style "stroke:rgb(255,0,0);stroke-width:2", markerEnd "url(#arrow)", onArcClick arc.id] []    
                     Svg.path [d (renderPath model sn tn arc.breakPoints), stroke "red", strokeWidth "2", fill "none", Svg.Attributes.style "stroke:rgb(255,0,0);stroke-width:2", markerEnd "url(#arrow)", onArcClick arc.id ][]
                   ]
                   ++ if arc.isSelected then
                         [
                           --line [x1 (toString sn.x), y1 (toString sn.y), x2 (toString tn.x), y2 (toString tn.y), 
                           --     Svg.Attributes.style "stroke:rgb(0,0,0);stroke-width:3", onAddBreakPoint arc.id] []
                           Svg.path [d (renderPath model sn tn arc.breakPoints), stroke "balck", strokeWidth "3", fill "none", Svg.Attributes.style "stroke:rgb(0,0,0);stroke-width:3", onAddBreakPoint arc.id ][]
                         ] ++ (renderBreakPoints model arc arc.breakPoints)
                       else
                         []
                 Nothing ->
                    Debug.crash "No target node"
             Nothing ->
                Debug.crash "No sorce node"

renderPath: Model -> Node -> Node -> List BreakPoint -> String
renderPath model startNode endNode breakPoints =
    "M"
    ++ (toString startNode.x) ++ " " ++ (toString startNode.y) 
    ++  (case breakPoints of
        [] ->
          ""
        _ ->
          " L" ++  String.join " L" (List.map  (\bp -> toString (bp.x - model.editor.x) ++ " " ++ toString (bp.y - model.editor.y)) breakPoints))
    ++" L"++(toString endNode.x) ++ " " ++(toString endNode.y)
    ++ "M"
renderBreakPoints : Model -> ArcDef  -> List BreakPoint -> List (Svg.Svg Msg)
renderBreakPoints model arc breakPoints =
  List.map (\bp -> circle [cx (toString (bp.x - model.editor.x)), cy (toString (bp.y - model.editor.y)), r "4", onMouseDownBreakPoint arc.id bp.id][]) breakPoints


renderNode : Node -> List (Svg.Svg Msg)
renderNode node =
    case node.nodeType of
      TRANSITION ->
        transition node  
      PLACE ->
        place node
      NOT_A_NODE ->
        []

    

transition : Node -> List (Svg.Svg Msg) 
transition trans =
    [rect [x (toString trans.x), y (toString trans.y), width ((toString trans.width)++"px"), height (toString trans.height), fill "white", stroke "black", strokeWidth "1",  onEditorClick (Just trans.id), onMouseDown trans.id] []
    ,Svg.text_ [
      textAnchor "middle", x (toString ((toFloat trans.x) +  ((toFloat trans.width) / 2)))
      , y (toString ((toFloat trans.y) +  ((toFloat trans.height) / 2)))
      , width (toString (trans.width - 2))] [Svg.text trans.label]]
    ++ case trans.isSelected of 
        True -> [
          rect [x (toString (trans.x - 2)), y (toString (trans.y - 2)), width ((toString (trans.width + 4))++"px"), height (toString (trans.height+ 4)), stroke "black", strokeWidth "1", fillOpacity "0", strokeDasharray "2"] []
         ,rect [x (toString (trans.x + trans.width + 2)), y (toString (trans.y + trans.height + 2)), width "4", height "4", stroke "black", strokeWidth "1", onResizeMouseDown trans.id][]
        ]
        False -> []
    
place : Node -> List (Svg.Svg Msg)
place pl = 
  let 
    radius = ((toFloat (pl.width + pl.height)/4))
  in
    [
      Svg.text_ [
        textAnchor "middle", x (toString pl.x)
        , y (toString pl.y)
        , width (toString (pl.width - 2))] [Svg.text pl.label]
        , circle [cx (toString pl.x), cy (toString pl.y), r (toString radius), stroke "black", strokeWidth "1", fillOpacity "0", onClick (ClickNode pl.id), onEditorClick (Just pl.id), onMouseDown pl.id] []
        , text_ [textAnchor "middle", 
                  x (toString ((toFloat pl.x) + (radius + 1) ) )
                , y (toString ((toFloat pl.y) - (radius + 1) ) )
                ] [Svg.text (toString pl.tokens)]
    ]
    ++ case pl.isSelected of 
        True -> 
          [
            circle [cx (toString pl.x), cy (toString pl.y), r (toString ((toFloat ((pl.width + pl.height))/4) +2)), stroke "black", strokeWidth "1", fillOpacity "0", strokeDasharray "2"][]
            ,rect [x (toString (pl.x + pl.width  +2)), y (toString (pl.y + pl.height +2)), width "4", height "4", stroke "black", strokeWidth "1", onResizeMouseDown pl.id][]
         ]
        False -> []
arc =
    []

onMouseDown : Int -> Html.Attribute Msg
onMouseDown id =
    on "mousedown" (Decode.map (DragStart id) Mouse.position)

onMouseDownBreakPoint : Int -> Int -> Html.Attribute Msg
onMouseDownBreakPoint arcId bpId=
    on "mousedown" (Decode.map (DragBreakPointStart arcId bpId) Mouse.position)

onAddBreakPoint:  Int -> Html.Attribute Msg
onAddBreakPoint id =
  onWithOptions "click" {stopPropagation=True, preventDefault=True}  (Decode.map (AddBreakPoint id) Mouse.position)

onArcClick:  Int -> Html.Attribute Msg
onArcClick  id =
    onWithOptions "click" {stopPropagation=True, preventDefault=True}  (Decode.succeed (SelectArc id))

onEditorClick:  Maybe Int -> Html.Attribute Msg
onEditorClick  id =
    onWithOptions "click" {stopPropagation=True, preventDefault=True}  (Decode.map (ApplyTool id) Mouse.position)

onResizeMouseDown id =
  on "mousedown" (Decode.map (ResizeStart id) Mouse.position)
-- onMouseUp =
--     on "mouseup" (Decode.map DragEnd Mouse.position) 
