module Attributes exposing (attributeView)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Types exposing (..)
import PnSemantics exposing (transitionCanFire)

attributeView : Model -> Html.Html Msg 
attributeView model = 
    table [id "attributes"
            , Html.Attributes.style [ ("position", "absolute")
                                    , ("left", (toString (model.editor.x + model.editor.width))++"px")
                                    , ("top", (toString model.editor.y) ++"px")
        ]] (getAttributeRows model)

getAttributeRows: Model -> List (Html Msg)
getAttributeRows model = 
    let
      selectedNode = List.head (List.filter (\x ->  x.isSelected) model.model.nodes)
    in
        case selectedNode of
          Just node ->
            getAttributesForNode node model
          Nothing ->
            []

getAttributesForNode: Node -> Model -> List (Html Msg)
getAttributesForNode node model =
    [tr [] ([
        td [][ text "Label"]
        ,td [] [
              input [value node.label, onInput (ChangeLabel node.id) ] []    
        ] 
    ] 
    )]
    ++
    case node.nodeType of
        PLACE ->
            [tr [] [
                td [][ text "Tokens"]
            ,   td [] [input [value (toString node.tokens), onInput (ChangeTokens node.id) ] []]]
               
            ]
        _ ->
            [tr [][
                td [] [
                    button [onClick (Fire node.id), disabled (not (transitionCanFire model node.id))]  [text "fire"] 
                ]
            ]]

