module Main exposing (..)

import App exposing (..)
import Html exposing (programWithFlags)
import Update exposing (..)
import Types exposing (..)



main : Program Never Model Msg
main =
    Html.program { view = view, init = init, update = Update.update, subscriptions = subscriptions }




