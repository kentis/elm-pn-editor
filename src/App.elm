module App exposing (..)
import Types exposing (..)

import Pallette exposing (..)
import Html exposing (Html, div)
import Editor exposing (..)
import Update exposing (..)
import Attributes exposing (attributeView)
--import Mouse exposing(moves, ups)

init : ( Model, Cmd Msg )
init  =
    ( {  selected = 0, dragged = 0, drag = Nothing
    , resized = 0
    ,resize = Nothing
    , model = {nodes =[
          {id=1, label="hei", nodeType = PLACE, x=15, y=50, width=10, height=10, isSelected = False, isDragged = False, tokens=0}
        , {id=2, label="hallo", nodeType=TRANSITION, x=75, y=70, width=50, height=50, isSelected = False, isDragged = False, tokens=0}
    ]
    , arcs=[
        {id=1, source=1, target=2, isSelected=False, breakPoints=[]}
    ]
    , metaModel=Nothing
    }
    , editor = {x = 250, y = 50, height=1000, width = 1200}
    , pallette = {x=0, y=50, height=1000, width=250, elements = [
                                                                    {id = 1, text="Place", nodeType = PLACE, selected = False, tool=CREATE_NODE}
                                                                ,   {id = 2, text = "Transition", nodeType = TRANSITION, selected = False, tool=CREATE_NODE}
                                                                ,   {id = 3, text = "Arc", nodeType = NOT_A_NODE, selected = False, tool=CREATE_ARC}
                                                                ] } 
    , arcStart= Nothing
    , dragType = NONE
    , draggedBp=0}, Cmd.none )

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    Update.update msg model


view : Model -> Html Msg
view model =
    div []
        [ --img [ src model.logo ] []
        --, div [] []
        --, 
          pallette model
        , editorView model
        , attributeView model
        ]



