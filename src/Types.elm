module Types exposing (..)
import Mouse exposing (Position)
type alias Model =
    { 
      model : ModelDef
    , editor : EditorDef
    , pallette: PalletteDef
    , selected: Int
    , dragged: Int
    , draggedBp: Int
    , dragType : DragType
    , drag : Maybe Drag
    , resized: Int
    , resize : Maybe ReSize
    , arcStart : Maybe Int
    }

type DragType =
   DRAG_NODE
  |DRAG_BREAKPOINT
  |NONE

type alias ModelDef = 
  {
    nodes : List Node
  , arcs : List ArcDef
  , metaModel : Maybe MetaModel
  }

type MetaModel = MetaModel ModelDef

type alias EditorDef = 
  {
    x: Int
  , y: Int
  , width : Int
  , height: Int
  }


type alias PalletteDef = 
  {
    x: Int
  , y: Int
  , width : Int
  , height: Int
  , elements : List PalletteElement
  }

type alias PalletteElement =
  {
    id : Int
  , text: String
  , selected: Bool
  , nodeType: NodeType
  , tool: Tool
  }
type alias Drag =
    { start : Position
    , current : Position
    }

type alias ReSize =
    { start : Position
    , startNode : Node
    }


type Tool =
    CREATE_NODE | CREATE_ARC

type Msg
    = NoOp
    | Fire Int
    | ClickNode Int
    | DragStart Int Position 
    | DragAt DragType Position
    | DragEnd Position
    | DragBreakPointStart Int Int Position 
    | DragBreakPointAt Position
    | DragBreakPointEnd Position
    | ResizeStart Int Position
    | ResizeAt Position
    | ResizeEnd Position
    | ApplyTool (Maybe Int) Position
    | SelectElement Int
    | SelectArc Int
    | ChangeLabel Int String
    | ChangeTokens Int String
    | AddBreakPoint Int Position --Add breakpoint on arc id (Int) at Position


type alias ArcDef = 
  {
    id : Int
  , source : Int
  , target : Int
  , isSelected: Bool
  , breakPoints: List BreakPoint
  }

type alias BreakPoint =
  {
    id: Int
  , x: Int
  , y: Int
  }

type NodeType =
    PLACE | TRANSITION | NOT_A_NODE

type alias Node = {
        id: Int
        , nodeType: NodeType
        , label: String
        , x: Int
        , y: Int
        , width: Int
        , height: Int
        , isSelected: Bool
        , isDragged: Bool
        , tokens: Int
    }
