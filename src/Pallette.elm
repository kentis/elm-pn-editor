module Pallette exposing (..)
import Utils exposing(ts)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Update exposing (..)
import Types exposing (..)

pallette model =
    ul [Html.Attributes.style [ ("position", "absolute")
                              , ("left", ((ts model.pallette.x)++"px"))
                              , ("top", ((ts model.pallette.y)++"px"))
                              , ("width", ((ts model.pallette.width)++"px"))
                              , ("height", ((ts model.pallette.height)++"px"))
                              , ("margin", "0")
                              , ("padding", "0")
                              , ("text-align", "left")
                              , ("border-right","solid")]
       ]
       (List.map renderElement model.pallette.elements)
    {--[
        li [] [ text "Place" ],
        li [] [ text "Transition" ]
    ]--}

renderElement : PalletteElement -> Html Msg
renderElement element =
    li [style (getElementStyle element), onClick (SelectElement element.id) ] [ text element.text ]

getElementStyle : PalletteElement -> List (String, String)
getElementStyle element =
    [
        ("background-color", case element.selected of
                    True ->
                        "gray"
                    _ ->
                        "white"
        )
    ]